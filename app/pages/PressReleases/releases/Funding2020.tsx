import * as React from "react";
import * as styles from "./Releases.scss";
import classNames from "classnames";
import { inject, observer } from "mobx-react";

import LayoutContainer from "components/LayoutContainer";
import TypeHeading from "components/TypeHeading";
import Section from "components/Section";
import Helmet from "react-helmet";
import MediaCard from "components/MediaCard";
import Text from "components/Text";

@inject("main")
@observer
class Funding2019 extends React.Component {
  public render() {
    return (
      <div>
        <Helmet title="Newsroom" />
        <Section>
          <LayoutContainer classes={styles.Vision}>
            <div className={styles.VisionBody}>
              <Text.Paragraph type="visionHeading">
                Sia Announces $3M Seed Round to Accelerate Development and Adoption of the Web3 platform Skynet
              </Text.Paragraph>

              <Text.Paragraph>
                <i>Paradigm leads the round of financing; Nebulous to solely focus on Skynet</i>
              </Text.Paragraph>

              <Text.Paragraph bold>Boston, MA – September 22nd, 2020</Text.Paragraph>

              <Text.Paragraph>
                Nebulous, the company building the{" "}
                <a href="https://sia.tech" target="_blank">
                  Sia
                </a>{" "}
                decentralized cloud storage platform and the{" "}
                <a href="https://siasky.net" target="_blank">
                  Skynet
                </a>{" "}
                application hosting platform, today announced it has closed a $3M funding round led by Paradigm with
                participation from Bain Capital Ventures, Bessemer Venture Partners, A.Capital, Collaborative Fund,
                Dragonfly Capital Partners, Hack VC, INBlockchain, First Star Ventures, and other notable investors. The
                round will help the company scale and accelerate the development and adoption of Skynet.
              </Text.Paragraph>

              <Text.Paragraph>
                Today’s internet monopolized by tech giants struggles with fundamental challenges in privacy,
                reliability, and has excessive control over our data and the way we interact with the world. Skynet
                enables high speed, low cost, and superior infrastructure to serve as the storage foundation for{" "}
                <a href="https://blog.sia.tech/the-war-for-a-free-internet-c0a7fcc00c46" target="_blank">
                  a free Internet
                </a>
                . Where data is globally accessible, user-controlled, censorship-resistant, and not fragmented in walled
                gardens.
              </Text.Paragraph>

              <Text.Paragraph>
                Skynet empowers developers to deploy applications to a decentralized network in just minutes and be
                immediately available to everyone across the world. Importantly, end-users can directly access content
                on Skynet without needing to run full nodes or deal with cryptocurrencies. It paves the path for more
                powerful applications, more dynamic user experiences, and disintermediates the web to ensure that
                publishers and creators are paid the full amount that they deserve. Skynet enables the decentralized web
                ecosystem to realize the full potential of a free internet.
              </Text.Paragraph>

              <Text.Paragraph>
                “The possibilities created by Skynet blow our minds. The project has enabled a growing ecosystem of
                builders to quickly prototype censorship-resistant applications and interactive websites including
                interfaces to Ethereum smart contracts like Uniswap,” said Dan Robinson, Research Partner at Paradigm.
                “We’re investing in the Skynet team because we believe they have the right combination of talent,
                experience, and community to shepherd us into the era of the Decentralized Web.”
              </Text.Paragraph>

              <Text.Paragraph>
                Nebulous{" "}
                <a href="https://blog.sia.tech/skynet-bdf0209d6d34" target="_blank">
                  launched
                </a>{" "}
                the Skynet platform back in February 2020 and since then has seen accelerated growth in developer
                interest and usage. More than 1.6 million files have been uploaded and shared amounting to 10+ TB of
                data using Skynet. A thriving community of developers has built over 100 applications in the span of a
                few months including a Video Streaming app{" "}
                <a href="https://skylive.coolhd.hu/" target="_blank">
                  SkyLive
                </a>
                , Blogging apps like{" "}
                <a href="https://siasky.net/hns/wakio" target="_blank">
                  Wakio
                </a>{" "}
                and{" "}
                <a href="https://siasky.net/DADAgjHeqGnQ1hIEET54YyIZhBN_oz51HMogZsLiS60KTA/" target="_blank">
                  Skyblog Builder
                </a>
                , Video & Image gallery app{" "}
                <a href="https://skygallery.xyz/" target="_blank">
                  SkyGallery
                </a>
                , and a Decentralized AppStore{" "}
                <a href="https://github.com/redsolver/skydroid" target="_blank">
                  Skydroid
                </a>
                . The full list of Skynet Apps can be explored at the{" "}
                <a href="https://siasky.net/hns/skyapps/" target="_blank">
                  Skynet AppStore
                </a>
                . We believe the next generation of Twitter, Medium, TikTok will be built on Skynet.
              </Text.Paragraph>

              <Text.Paragraph>
                By building on the{" "}
                <a href="https://sia.tech" target="_blank">
                  Sia network
                </a>
                , Skynet delivers a 10x reduction in storage costs and a 100x reduction in bandwidth costs when compared
                to centralized providers, without sacrificing performance or reliability. Skynet achieves 1 gigabit per
                second in download and uploads speeds, with more improvements coming in future releases.
              </Text.Paragraph>

              <Text.Paragraph>
                “For the first time, the decentralized web feels within reach. We’re not talking about a web that is
                technically feasible yet crippled compared to the centralized alternatives, we are talking about a
                decentralized web that will become an unstoppable force” said David Vorick, Nebulous CEO and Skynet Lead
                Developer. “Ten years from now, using centralized applications instead of decentralized applications
                will feel like using a fax machine instead of email.”
              </Text.Paragraph>

              <Text.Paragraph>
                In addition to the funding news, Nebulous is announcing its{" "}
                <a href="https://blog.sia.tech/skynet-the-future-of-nebulous-c9922eb53456" target="_blank">
                  rebranding
                </a>{" "}
                to Skynet to solely focused on developing and growing the Skynet ecosystem. Next-generation applications
                are already being built on Skynet with more developers joining the community every day. Skynet is
                different from any platform or technology than has ever been built before. Because of that, we are
                thrilled to be doubling down on what we know will become the future of the Internet.
              </Text.Paragraph>

              <Text.Paragraph>
                Interested in Skynet? Learn more at{" "}
                <a href="https://siasky.net/" target="_blank">
                  siasky.net
                </a>
                , join us on{" "}
                <a href="https://discord.gg/sia" target="_blank">
                  Discord
                </a>
                , or email us at hello@sia.tech.
              </Text.Paragraph>

              <Text.Paragraph>
                Want to help us re-decentralize the Internet? Nebulous is hiring for positions in marketing and
                developer evangelism. Learn more about our projects and{" "}
                <a href="https://jobs.lever.co/nebulous" target="_blank">
                  apply today
                </a>
                !
              </Text.Paragraph>

              <TypeHeading level={6}>About Nebulous</TypeHeading>
              <Text.Paragraph>
                <a href="https://nebulous.tech" target="_blank">
                  Nebulous
                </a>{" "}
                builds uncompromising software infrastructure for the decentralized internet. This includes{" "}
                <a href="https://sia.tech" target="_blank">
                  Sia
                </a>
                , the leading decentralized cloud storage platform, and{" "}
                <a href="https://siasky.net/" target="_blank">
                  Skynet
                </a>
                , a content and application hosting platform.
              </Text.Paragraph>

              <Text.Paragraph>
                Nebulous defines uncompromising infrastructure as scalable, trustless, secure, and – most important –
                fully decentralized. In a blockchain industry filled with hype but lacking substance, Nebulous stands
                out as one of the few deeply technical teams that consistently deliver real products with significant
                potential.
              </Text.Paragraph>

              <Text.Paragraph>
                Nebulous, Inc. was founded in 2014 and is headquartered in Boston. Before the round led by Paradigm,
                Nebulous was funded by Bain Capital Ventures, A.Capital, Bessemer Venture Partners, Dragonfly Capital
                Partners, First Star Ventures, and other notable investors.
              </Text.Paragraph>

              <TypeHeading level={6}>About Paradigm</TypeHeading>
              <Text.Paragraph>
                <a href="https://www.paradigm.xyz/" target="_blank">
                  Paradigm
                </a>{" "}
                is a crypto-focused investment firm based in San Francisco founded by Fred Ehrsam and Matt Huang. The
                firm makes investments focusing on crypto and blockchain technologies from the earliest stages of
                ideation through maturity. Prior to founding Paradigm, Fred co-founded Coinbase, the largest
                cryptocurrency company in the US, and Matt was a partner at Sequoia Capital focusing on early-stage
                venture investments including leading the firm’s cryptocurrency efforts.
              </Text.Paragraph>
            </div>
          </LayoutContainer>
        </Section>
      </div>
    );
  }
}

export default Funding2019;
